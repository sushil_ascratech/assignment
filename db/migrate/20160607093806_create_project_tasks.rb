class CreateProjectTasks < ActiveRecord::Migration
  def change
    create_table :project_tasks do |t|
      t.string :name
      t.integer :project_id
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
