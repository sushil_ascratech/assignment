class AddStatusToProjrctTask < ActiveRecord::Migration
  def change
  	add_column :project_tasks, :status, :string, :default => "New"
  end
end
