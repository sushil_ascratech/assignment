class ProjectTask < ActiveRecord::Base
  belongs_to :project
  belongs_to :user

  def get_developer_email
  	User.find_by_id(self.user_id).email rescue ""
  end

  def self.get_all_task(projects,user)
  	if user.role == "developer"
  	  return ProjectTask.where(:user_id => user.id)
    elsif user.role == "admin"
      return projects.project_tasks
    else
    	return []
    end
  end
end