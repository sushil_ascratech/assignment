class Project < ActiveRecord::Base
  has_many :project_tasks
  has_many :users, through: :project_tasks

  def get_all_task
  	self.project_tasks
  end

  
end