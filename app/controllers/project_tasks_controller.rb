class ProjectTasksController < ApplicationController
  before_action :authenticate_user!
  before_action :find_project
  
  def index

    @project_tasks = ProjectTask.get_all_task(@project,current_user)
  end

  def new
    @project_task = @project.project_tasks.new    
  	@users = User.where(:role => "Developer")
  end

  def create
    @project_task = @project.project_tasks.new(project_task_params)
    if @project_task.save
      redirect_to project_project_tasks_path
    else
      render :new
    end
  end

  def edit
  	@project_task = @project.project_tasks.find_by_id(params[:id])
  	@users = User.where(:role => "Developer")
  end

  def update
  	@project_task = @project.project_tasks.find_by_id(params[:id])
    if @project_task.update(project_task_params)
      if project_task_params[:status].present?
        redirect_to project_tasks_path
      else
        redirect_to project_project_tasks_path
      end
    else
      render :edit
    end
  end

  def destroy
  	
  end

  protected
    
  def project_task_params
    params[:project_task].permit!
  end

  def find_project
  	@project = Project.find_by_id(params[:project_id])
  end
end
